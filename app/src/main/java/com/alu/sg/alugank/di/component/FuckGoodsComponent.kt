package com.alu.sg.alugank.di.component


import com.alu.sg.alugank.fragment.AndroidFragment
import com.alu.sg.alugank.fragment.GirlFragment
import com.alu.sg.alugank.fragment.IosFragment
import com.alu.sg.alugank.inter.FuckGoodsContract
import dagger.Module
import dagger.Provides
import dagger.Subcomponent

/**
 * Created by alu
 */
@Subcomponent(modules = arrayOf(FuckGoodsModule::class))
interface FuckGoodsComponent {
    fun inject(fragment: AndroidFragment)
    fun inject(fragment: IosFragment)
    fun inject(fragment: GirlFragment)
}

@Module
class FuckGoodsModule(private val mView: FuckGoodsContract.View) {
    @Provides fun getView() = mView
}