package com.alu.sg.alugank.activity


import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import com.alu.sg.alugank.R
import com.alu.sg.alugank.base.MainActivity
import kotlinx.android.synthetic.main.activity_welcome.*

/**
 * Created by alu on 2017/7/5.
 */
class WelcomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        main.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            launcher_view.start()
            Handler().postDelayed({
                MainActivity.actionStart(this)
                finish()
            }, 4000)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}