package com.alu.sg.alugank

import android.content.Context
import android.content.Intent
import android.net.Uri

/**
 * Created by Alu on 2017/6/9.
 * 版本：V1.0
 */
object GankRouter{
    fun router(context: Context,uri : String){
        val intent = Intent()
        intent.data = Uri.parse(uri)
        intent.action = Intent.ACTION_VIEW
        context.startActivity(intent)
    }

}