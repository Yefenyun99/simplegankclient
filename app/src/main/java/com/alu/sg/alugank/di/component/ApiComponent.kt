package com.alu.sg.alugank.di.component


import com.alu.sg.alugank.App
import com.alu.sg.alugank.di.module.ApiModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by alu
 */
@Singleton
@Component(modules = arrayOf(ApiModule::class))
interface ApiComponent { 

    fun inject(app: App)

    fun plus(module: FuckGoodsModule): FuckGoodsComponent

}

