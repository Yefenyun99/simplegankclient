package com.alu.sg.alugank.di.module


import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * Created by alu
 */
@Module
class AppModule(private val context: Context) {
    @Provides fun provideContext() = context
}
