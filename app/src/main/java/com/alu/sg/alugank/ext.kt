package com.alu.sg.alugank

import android.content.Context
import android.widget.Toast
import com.alu.sg.alugank.App

/**
 * Created by alu
 */
fun Context.getMainComponent() = App.instance.apiComponent

fun Context.toast(msg:String,length:Int = Toast.LENGTH_SHORT){
  Toast.makeText(this, msg, length).show()
}