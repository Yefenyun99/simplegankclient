package com.alu.sg.alugank.present

import android.util.Log
import com.alu.sg.alugank.inter.FuckGoodsContract
import com.alu.sg.alugank.model.FuckGoodsModel
import rx.android.schedulers.AndroidSchedulers
import javax.inject.Inject

/**
 * Created by Alu on 2017/6/14.
 * 版本：V1.0
 */
class GirlPresenter
@Inject constructor(private val mModel: FuckGoodsModel,
                    private val mView: FuckGoodsContract.View)
    : FuckGoodsContract.Presenter, BasePresenter() {


    override fun getData(page: Int, type: String) {
        addSubscription(mModel.getData(page, type).observeOn(AndroidSchedulers.mainThread())
                .subscribe({

                    res ->
                    if (!res.error) {
                        mView.setData(res.results)
                    }

                }, { e -> Log.e("wing", "error android Presenter" + e.message) }))
    }

}