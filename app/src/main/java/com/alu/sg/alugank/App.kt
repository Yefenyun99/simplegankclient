package com.alu.sg.alugank

import android.app.Application
import com.alu.sg.alugank.di.component.ApiComponent
import com.alu.sg.alugank.di.component.DaggerApiComponent
import com.alu.sg.alugank.di.module.ApiModule
import com.alu.sg.alugank.di.module.AppModule
import javax.inject.Inject

/**
 * Created by Alu on 2017/7/11.
 * version 1.0
 */
class App : Application() {
    init {
        instance = this
    }

    @Inject lateinit var apiComponent: ApiComponent
    override fun onCreate() {
        super.onCreate()
        DaggerApiComponent
                .builder()
                .apiModule(ApiModule())
                .appModule(AppModule(this))
                .build()
                .inject(this)
    }

    companion object {
        lateinit var instance: App
    }
}