### 效果图
![输入图片说明](http://upload-images.jianshu.io/upload_images/3335448-36afafb18ced0e0b.gif?imageMogr2/auto-orient/strip "在这里输入图片标题")
### 开发语言:
java（BaseFragment,BaseActivity）+kotlin(其余页面)混编
### 用到的开源库
    rxjava
    rxandroid
    retrofit2
    databinding
    glide
    anko
    deeplink
### 致谢以及分享：

    http://oo94hcyew.bkt.clouddn.com/kotlin-in-chinese.pdf
    http://www.mamicode.com/info-detail-1172799.html
    http://www.kotlincn.net/docs/reference/idioms.html
    https://zhuanlan.zhihu.com/p/23101437
    http://gank.io/download

### 更新：
    2017-7-5：加入好玩的欢迎页
    2017-7-11：处理偶尔内存溢出的问题
    2017-9-26：使用Dagger2注解解耦 

